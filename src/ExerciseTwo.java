/* 
 * Name: Durjoy Barua 
 * Student ID: 040919612 
 * Course & Section: CST8132 302 
 * Assignment: Lab 2 * Date: Sept 19, 2018
 */
public class ExerciseTwo {
	public int[][] myDoubleDArray = new int[6][10];//Create instance variable

	//Main method of the program
	public static void main(String[] args) {

		ExerciseTwo DoubleDArrayprint = new ExerciseTwo();


		DoubleDArrayprint.printArrayValues();
		DoubleDArrayprint.displayArrayTotal();
	}//End of main method

	//Default constructor that assign 60 numbers to the array
	public ExerciseTwo() {
		//Generate the numbers for the 2D Array
		int p = 1;

		for(int i = 1; i <= myDoubleDArray.length; i++) {
			for(int j = 1; j <= myDoubleDArray[0].length; j++) {
				myDoubleDArray[i-1][j-1] = p;
				p++;

			}
		}
	}//End of Default constructor

	//Print all the numbers of the array into the screen
	public void printArrayValues() {

		System.out.println("myValue = {");
		for(int row = 0; row < myDoubleDArray.length; row++) {
			System.out.print("{");
			for(int col = 0; col < myDoubleDArray[0].length; col++) {
				System.out.print(myDoubleDArray[row][col]);
				if(col != 9) {
					//Place the comma after every number and remove it at the end of the block
					System.out.print(",");
				}
			}
			System.out.println("},");
		}
		System.out.print("}");
	}//End of prntArrayValue method


	//Print the summation of the 2D Array into the screen
	public void displayArrayTotal() {
		int sum = 0;
		//Loop for calculating each number continuously
		for(int[] p: myDoubleDArray) {
			for(int k: p) {
				sum += k;
			}
		}

		//Print the result
		System.out.print("\nThe sum of all elements of myArray is: "+sum);
	}//End of DisplayArray Total method
}
